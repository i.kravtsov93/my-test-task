#Build image
docker build -t my-nginx:latest .
#Push image to remote repository
docker image tag my-nginx:latest registry-host:5000/my-nginx:latest
docker image push registry-host:5000/my-nginx:latest